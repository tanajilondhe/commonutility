package com.tml.bean.util;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Logger;

public class BeanUtil {

	private static final Logger LOGGER = Logger.getLogger(BeanUtil.class
			.getName());

	private static final Class<?>[] EMPTY_ARRAY = {};

	// Suppresses default constructor, ensuring non-instantiability.
	private BeanUtil() {

	}

	public static List<Class<?>> getAnnotatedClasses(Class<?>[] classes,
			Class<? extends Annotation> annotationClass) {
		List<Class<?>> list = null;

		for (Class<?> clazz : classes) {
			for (Field field : clazz.getDeclaredFields()) {
				if (field.isAnnotationPresent(annotationClass)) {
					if (list == null) {
						list = new ArrayList<>();
					}
					list.add(clazz);
				}
			}
		}

		if (list == null) {
			return Collections.emptyList();
		}

		return list;
	}

	public static void copyNullProperties(Object source, Object destination) {

		if (source != null
				&& destination != null
				&& source.getClass().getCanonicalName()
						.equals(destination.getClass().getCanonicalName())) {

			Field[] sourceFields = source.getClass().getDeclaredFields();
			try {
				setPropertyValue(source, sourceFields, destination,
						destination.getClass());
			} catch (NoSuchFieldException | SecurityException
					| IllegalArgumentException | IllegalAccessException e) {
				LOGGER.warning(e.getMessage() + " : " + e);
			}
			Class<?> superClass = source.getClass().getSuperclass();

			while (superClass != null) {
				Field[] superClassFields = superClass.getDeclaredFields();
				try {
					setPropertyValue(source, superClassFields, destination,
							superClass);
				} catch (NoSuchFieldException | SecurityException
						| IllegalArgumentException | IllegalAccessException e) {
					LOGGER.warning(e.getMessage() + " : " + e);
				}
				superClass = superClass.getSuperclass();
				if (superClass.equals(Object.class)) {
					superClass = null;
				}
			}
		} else {
			throw new IllegalArgumentException(
					"Source and Destination class types are different");
		}
	}

	private static void setPropertyValue(Object source, Field[] sourceFields,
			Object destination, Class<?> destinationClass)
			throws NoSuchFieldException, SecurityException,
			IllegalArgumentException, IllegalAccessException {

		for (Field field : sourceFields) {
			field.setAccessible(true);
			Object srcFieldValue = field.get(source);
			Field destinationField = destinationClass.getDeclaredField(field
					.getName());
			destinationField.setAccessible(true);
			Object desFieldValue = field.get(destination);
			if (desFieldValue == null) {
				field.set(destination, srcFieldValue);
			}
		}
	}

	/**
	 * This method return all the java class inside the given package name
	 * 
	 * @param packageName
	 * @return
	 */
	public static Class<?>[] getClasses(String packageName) {

		ClassLoader classLoader = Thread.currentThread()
				.getContextClassLoader();
		assert classLoader != null;
		String path = packageName.replace('.', '/');
		try {
			Enumeration<URL> resources = classLoader.getResources(path);
			List<File> dirs = new ArrayList<>();
			while (resources.hasMoreElements()) {
				URL resource = resources.nextElement();
				dirs.add(new File(resource.getFile()));
			}
			List<Class<?>> classes = new ArrayList<>();
			for (File directory : dirs) {
				classes.addAll(findClasses(directory, packageName));
			}
			return classes.toArray(new Class[classes.size()]);
		} catch (IOException | ClassNotFoundException e) {
			LOGGER.warning(e.getMessage() + " : " + e);
		}
		return EMPTY_ARRAY;
	}

	private static List<Class<?>> findClasses(File directory, String packageName)
			throws ClassNotFoundException {

		List<Class<?>> classes = new ArrayList<>();
		if (!directory.exists()) {
			return classes;
		}
		File[] files = directory.listFiles();
		for (File file : files) {
			if (file.isDirectory()) {
				classes.addAll(findClasses(file,
						packageName + "." + file.getName()));
			} else if (file.getName().endsWith(".class")) {
				classes.add(Class.forName(packageName
						+ '.'
						+ file.getName().substring(0,
								file.getName().length() - 6)));
			}
		}
		return classes;
	}

}
