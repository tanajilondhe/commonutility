package com.tml.bean.util;

import java.util.Collection;

public class CollectionUtil {

	// Suppresses default constructor, ensuring non-instantiability.
	private CollectionUtil() {

	}

	public static boolean isEmpty(Collection<?> collection) {

		if (collection == null) {
			return true;
		}

		return collection.isEmpty();
	}
}
